# Prozess Management Projekt CBA2019

## Dateistruktur
|                    | Beschreibung                                                                                        |                                                                         |
|--------------------|-----------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------|
| camunda-data       | Enthält alle Ordner und Dateien für die persistente Speicherung der Prozesse und Prozess-Variablen. | [Camunda](#camunda)                                                     |
| mongo-data         | Enthält alle Ordner und Dateien für die persistente Speicherung der Datenbank.                      | [Datenbank](#datenbank)                                                 |
| node-code          | Code für die benutzerdefinierte Geschäftslogik von Service-Tasks.                                   | [External Service Tasks](#external-task-service)                       |
| python-code        | Code für das Deployment der Prozesse und Formulare nach Camunda.                                    | [Deployment](#deployment)                                               |
| docker-compose.yml | Enthält die Definition der Container-Architektur.                                                   | [Start und Umgang mit der Umgebung](#start-und-umgang-mit-der-umgebung) |

## Start und Umgang mit der Umgebung 
Aufgebaut wurde die Architektur der Anwendung auf Basis von Containern. Die folgenden Containern werden für den Betrieb der Anwendung benötigt: 

| Container     | Port | Aufgabe                                                       |
|---------------|------|---------------------------------------------------------------|
| camunda       | 8080 | Lauffähige vollständige Camunda BPM Platform                  |
| node          |      | Logikimplementierung für Service Tasks                        |
| python        |      | Deploymentfunktionen                                          |
| mongo         |      | Datenbank um Formulardaten zu speichern und abrufen zu können |
| mongo-express | 8081 | GUI für die Mongo-DB                                          |

1. Um die Umgebung zu Starten muss zunächst sichergestellt werden, dass Docker und Docker Compose installiert ist. 
2. Innerhalb Projektverzeichnisses (per Shell oder Terminal) folgenden Befehl ausführen: 
```
docker-compose up --build
```
3. Die einzelnen Container mit ihren Abhängigkeiten werden automatisch gestartet. Nach dem Start können die Container `camunda` und `mongo-express` über die obenstehenden Ports ansprechen.

## Links und Benutzer
- Annahme: Die Anwendung wird lokal auf einem Rechner ausgeführt und aufgerufen.

#### Camunda 
Die drei Camunda-Webapps (Takslist, Cockpit und Admin) sind über die Landing-Page erreichbar:
[http://localhost:8080/camunda-welcome/index.html](http://localhost:8080/camunda-welcome/index.html)

Die Default-Credentials für den Admin sind: 
- Unsername: `demo`
- Passwort: `demo`

Für Projekt wurden weitere Benutzer erstellt, die an verschiedenen Prozessen beteilligt sind. 
Die Benutzer sind lediglich für die Webapp Tasklist freigeschaltet. Diese ist erreichbar unter: 
[http://localhost:8080/camunda/app/tasklist/](http://localhost:8080/camunda/app/tasklist/)

| Username         | Passwort          | Prozesse                                                   |
|------------------|-------------------|------------------------------------------------------------|
| `restaurant`     | `restaurant`      | Gäste registrieren                                         |
| `gast`           | `gast`            | Gäste registrieren                                          |
| `gesundheitsamt` | `gesundheitsamt`  | Kontaktpersonenliste erstellen, Kontaktperson kontaktieren |
| `infizierter`    | `infizierter`     | Kontaktpersonenliste erstellen                             |

#### Datenbank
Bei Mongo-DB handelt es sich um eine okumentenorientierte NoSQL-Datenbank. Hier werden die Formulardaten, die in den Prozessen entstehen gespeichert. 
Die Datenbank ist erreichbar unter folgendem Link: 
[http://localhost:8081/db/gpm/](http://localhost:8081/db/gpm/)

In der Collection `corona-form-data` werden die Daten der registrierten Gäste gespeichert.

## Prozesse 
Dss Projekt wurde im Rahmen der Vorlesung "Geschäftsprozessmanagement" erstellt. Ziel war es einen realen Prozess zu modellieren und zu implementieren. Im Rahmen der aktuellen Corona-Krise gibt es Auflagen für Restaurants, die Gäste mit ihren persönlichen Daten zu registrieren, um im Falle einer Infektion mögliche Kontaktpersonen nachverfolgen zu können. Der Gedanke dahinter ist, Infektionen so schnell wie möglich zu erkennen, um Maßnahmen einzuleiten (z.B. das Verhängen von Quarantäne) die die Ausbreitung des Virus verhindern.

Vereinfacht kann man das Ganze in drei Prozesse aufteilen: 
1. Gäste registrieren
    - Dient dem Erfassen der persönlichen Daten im Restaurant. Zusammen mit dem Datum und dem Zeitraum des Aufenthalt kann der anwesende Personenkreis bestimmt werden.
2. Kontaktpersonenliste erstellen 
    - Kommt es zu einer Infektion, meldet sich der Betroffene beim Gesundheitsamt. Hier startet nun ein Prozess, um mögliche Kontaktpersonen zu identifizieren. 
3. Kontaktperson kontaktieren
    - Wurden die Kontaktpersonen identifiziert, müssen diese Kontaktiert werden, damit sie sich auf den Virus testen lassen können, um eine Infektion zu bestätigen oder auszuschließen.

### Modelle 
Die (technischen) BPMN Modelle wurden im Camunda Modeler erstellt und sind unter folgenden Pfaden zu finden: 
1. Gäste registieren: 
    - `python-code/bpmn/corona-form.bpmn`
2. Kontaktpersonenliste erstellen: 
    - `python-code/bpmn/case-management.bpmn` 
3. Kontaktpersonen kontaktieren: 
    - `python-code/bpmn/contact-contact.bpmn`

## Deployment 
Die Funktionen für das Deployment befinden sich in `python-code/deploy.py`. Die Deployment-Funktionen können am Ende des Files aufgerufen werden. Im Anschluss kann das Deplyoment über `.....`.

### Deployment Funktionen für Camunda 

1. `deployToCamunda2()`
    - Deployment-Funktion für den Prozess "Gäste registrieren"
    - Deployment-Files: 
        - `python-code/bpmn/corona-form.bpmn`
        - `python-code/html/coron-form-init.html` 
        - `python-code/html/corona-form-eingabe.html`
        - `python-code/html/corona-form-logout.html`

2. `deployToCamunda2()` 
    - Deployment-Funktion für den Prozess "Kontaktpersonenliste erstellen"
    - Deployment-Files: 
        - `python-code/bpmn/case-management.bpmn`
        - `python-code/html/case-management-init.html`
        - `python-code/html/case-management-nachforschung.html`
        - `python-code/html/case-management-weitere-orte.html`
        - `python-code/html/case-management-kontaktliste-abholen.html`

3. `deployToCamunda3()`
    - Deployment-Funktion für den Prozess "Kontaktperson kontaktieren"
    - Deployment-Files: 
        - `python-code/bpmn/contact-contact.bpmn`
        - `python-code/html/contact-contact-init.html`
        - `python-code/html/contact-contact-phone.html`
        - `python-code/html/contact-contact-mail.html`
        - `python-code/html/contact-contact-mail-check.html`
        - `python-code/html/contact-contact-police.html`

### Hilffunktionen für die Interaktion mit Camunda 
1. `deleteAllProcessInstances()`
    - Funktion, um alle Laufenden Prozess Instanzen zu löschen. 

2. `deleteProcessDefinition(<Definition Key>)`
    - Funktion, um eine Prozessdefinition zu löschen.

### Deployment Funktionen für die Datenbank
1. `insertTestData()`
    - Funktionen, um Testdaten in die Datenbank zu schreiben. Die Testdaten sind in `python-code/guests.csv` zu finden und werden in die Datenbank `gpm` deployt. 

## External Task Service 
Für benutzerdefinierte Geschäftslogiken existiert der Node-Container. Die Business-Logiken für die einzelnen Tasks sind im File `node-code/woker.js` zufinden.

Damit die Geschäftslogik ausgeführt wird, wurden im BPMN-Modell für die jeweiligen Service Tasks sogenannte Topics definiert.
Die Client-Funktionen lauschen auf diese Topics und führen die Logik aus, sobald eine Service Tasks im Camunda BPMN ausgeführt werden soll: 

| Prozess                        | Tasks                       | Topic            | Beschreibung                                                                                             |
|--------------------------------|-----------------------------|------------------|----------------------------------------------------------------------------------------------------------|
| Gäste registrieren             | Startzeit eintragen         | `set-start-date` | Setzt das Startdatum und Uhrzeit des Restaurantbesuchs.                                                  |
| Gäste registrieren             | Endzeit eintragen           | `set-end-date`   | Setzt das Enddatum und Uhrzeit des Restaurantbesuchs.                                                    |
| Gäste registrieren             | Daten abspeichern           | `write-to-db`    | Schreibt die persönlichen Daten samt Start- und Endzeit in die Datenbank.                                |
| Kontaktpersonenliste erstellen | Kontaktpersonen extrahieren | `check-db`       | Extrahiert die Kontaktpersonen aus der Datenbank, die zur selben Zeit wie der Infizierte präsent waren.  |








