import requests 
import pymongo
import datetime
import random
import pandas as pd

def deployToCamunda1():

    #Deployment COVID-19 Formular erstellen

    base = "http://camunda:8080/engine-rest"

    url = base + "/deployment/create"

    data = {
        "deployment-name":"rest-test",
        "enable-duplicate-filtering":"false", 
        "deploy-changed-only":"false"
    }

    #files = {'file': open('diagram.bpmn's, 'rb')}
    files =  {
        'corona-form.bpmn': open('bpmn/corona-form.bpmn', 'rb'), 
        'corona-form-eingabe.html': open('html/corona-form-eingabe.html','rb'),
        'corona-form-logout.html': open('html/corona-form-logout.html','rb'),
        'corona-form-init.html': open('html/corona-form-init.html','rb')
    }

    #files =  {'test.html': open('test.html','rb')}

    x = requests.post(url, data=data, files=files)

    print(x.text)

def deployToCamunda2():

    base = "http://camunda:8080/engine-rest"

    url = base + "/deployment/create"

    data = {
        "deployment-name":"rest-test",
        "enable-duplicate-filtering":"false", 
        "deploy-changed-only":"false"
    }

    #files = {'file': open('diagram.bpmn', 'rb')}
    files =  {
        'case-management.bpmn': open('bpmn/case-management.bpmn', 'rb'), 
        'case-management-init.html': open('html/case-management-init.html','rb'),
        'case-management-nachforschung.html': open('html/case-management-nachforschung.html','rb'),
        'case-management-weitere-orte.html': open('html/case-management-weitere-orte.html','rb'),
        'case-management-kontaktliste-abholen.html': open('html/case-management-kontaktliste-abholen.html','rb')  
    }

    #files =  {'test.html': open('test.html','rb')}

    x = requests.post(url, data=data, files=files)

    print(x.text)


def deployToCamunda3():

    base = "http://camunda:8080/engine-rest"

    url = base + "/deployment/create"

    data = {
        "deployment-name":"rest-test",
        "enable-duplicate-filtering":"false", 
        "deploy-changed-only":"false"
    }

    #files = {'file': open('diagram.bpmn', 'rb')}
    files =  {
        'contact-contact.bpmn': open('bpmn/contact-contact.bpmn', 'rb'), 
        'contact-contact-init.html': open('html/contact-contact-init.html','rb'),
        'contact-contact-phone.html': open('html/contact-contact-phone.html','rb'),
        'contact-contact-mail.html': open('html/contact-contact-mail.html','rb'),
        'contact-contact-mail-check.html': open('html/contact-contact-mail-check.html','rb'),
        'contact-contact-police.html': open('html/contact-contact-police.html','rb')
    }

    #files =  {'test.html': open('test.html','rb')}

    x = requests.post(url, data=data, files=files)

    print(x.text)

def deleteProcessDefinition(process):
    base = "http://camunda:8080/engine-rest"

    url = base + "/process-definition/key/"+ process + "/delete"

    data = {
        "cascade":"true",
    }

    x = requests.delete(url, data=data)
    print(x.text)

def deleteAllProcessInstances():

    base = "http://camunda:8080/engine-rest"

    url = base + "/process-instance"

    data={}


    x = requests.get(url,data=data)
   

    x = x.json()

    print(x)

    for i in range(0,len(x)):
        id = x[i]["id"]
        url = base + "/process-instance/"+id
        requests.delete(url)




def deployDatabase(): 

    myclient = pymongo.MongoClient("mongo",27017)

    mydb = myclient["gpm"]

    mycollection = mydb["corona-form-data"]

    dblist = myclient.list_database_names()

    mydict = { "name": "John", "vorname": "Highway 37" }

    x = mycollection.insert_one(mydict)

    print(x.inserted_id)

    print(dblist)

def getRandomFutureTime(past):
    current_date_and_time = datetime.datetime.now()

    print("Current Datetime: "+ current_date_and_time.isoformat())    

    if(past):
        hours = random.randint(2,5)/10
        hours_added = datetime.timedelta(hours = hours) 
        future_date_and_time = current_date_and_time - hours_added
        print("Past Datetime: "+  future_date_and_time.isoformat())  
    else: 
        hours = random.randint(5,30)/10
        hours_added = datetime.timedelta(hours = hours)
        future_date_and_time = current_date_and_time + hours_added 
        print("Future Datetime: "+  future_date_and_time.isoformat())    
    
    return future_date_and_time

def getRelTables(tischnummer):
    if(tischnummer==1):
        return "2,4"
    elif(tischnummer==2):
        return "1,3,5"
    elif(tischnummer==3):
        return "2,6"
    elif(tischnummer==4):
        return "1,5"
    elif(tischnummer==5):
        return "2,4,6"
    elif(tischnummer==6):
        return "3,5"

def insertTestData2():
    myclient = pymongo.MongoClient("mongo",27017)

    mydb = myclient["gpm"]

    mycollection = mydb["corona-form-data"]

    #dblist = myclient.list_database_names()

    time = datetime.datetime.now().isoformat() 


    mydict1 = {
            name: 'Sulzer',
            vorname: 'Siegfried',
            email: 'siggi.sulzer@web.de',
            phone: '02604721836',
            restaurant: 'QMUH Burgergrill & Steakhouse',
            startdate: time,
            enddate: getRandomFutureTime(),
            tischnummer: 2,
            rel_tische: '1,3,5'
        }

    x = mycollection.insert_one(mydict)

    print(x.inserted_id)

def insertTestData():
    data = pd.read_csv("guests.csv", encoding='utf-16')

    print(data.head(10))

    for index, row in data.iterrows():

      

        mydict = {
            "name": str(row["name"]).replace(" ", ""),
            "vorname": str(row["vorname"]).replace(" ", ""),
            "email": str(row["email"]).replace(" ", ""),
            "phone": "0"+str(row["phone"]),
            "restaurant": 'QMUH Burgergrill & Steakhouse',
            "startdate": getRandomFutureTime(past=True),
            "enddate": getRandomFutureTime(past=False),
            "tischnummer":  row["tischnummer"],
            "rel_tische": getRelTables(row["tischnummer"])
        }

        myclient = pymongo.MongoClient("mongo",27017)

        mydb = myclient["gpm"]

        mycollection = mydb["corona-form-data"]

        x = mycollection.insert_one(mydict)
        print(x.inserted_id)



#deleteAllProcessInstances()
#deployToCamunda1()
#deployToCamunda2()
#deployToCamunda3()


#deployDatabase()


insertTestData()
#deleteProcessDefinition("Process_0bstdqq")



    
    


