const { Client, logger, Variables } = require('camunda-external-task-client-js');
const open = require('open');
const MongoClient = require('mongodb').MongoClient;

// configuration for the Client:
//  - 'baseUrl': url to the Process Engine
//  - 'logger': utility to automatically log important events
//  - 'asyncResponseTimeout': long polling timeout (then a new request will be issued)
const config = { baseUrl: 'http://camunda:8080/engine-rest', use: logger, asyncResponseTimeout: 10000 };

// create a Client instance with custom configuration
const client = new Client(config);

// susbscribe to the topic: 'charge-card'
client.subscribe('charge-card', async function({ task, taskService }) {
  // Put your business logic here

  // Get a process variable
  const amount = task.variables.get('amount');
  const item = task.variables.get('test');

  console.log(`Charging credit card with an amount of ${amount}€ for the item '${item}'...`);

  open('https://docs.camunda.org/get-started/quick-start/success');

  // Complete the task
  await taskService.complete(task);
});


// susbscribe to the topic: 'charge-card'
client.subscribe('test-charge', async function({ task, taskService }) {
    // Put your business logic here
  
    // Get a process variable
    const amount = task.variables.get('amount');
    const item = task.variables.get('test');
  
    console.log(`TEST Charging credit card with an amount of ${amount}€ for the item '${item}'...`);
  
    open('https://docs.camunda.org/get-started/quick-start/success');
  
    // Complete the task
    await taskService.complete(task);
  });


  // susbscribe to the topic: 'charge-card'
client.subscribe('check-form', async function({ task, taskService }) {
  // Put your business logic here

  console.log("start");

  const variables = new Variables();

  //variables.setTyped("approved", { type: "boolean", value: false});

  const name = task.variables.get('name');
  const vorname = task.variables.get('vorname');
  const email = task.variables.get('email');
  const phone = task.variables.get('phone');

  function isEmpty(value) {
    return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
  }

  function isNotEmpty(value){
    if(isEmpty(value)==true){
      return false;
    }
    else {
      return true;
    }
  }
  
  if(isNotEmpty(name) && isNotEmpty(vorname) && isNotEmpty(email) && isNotEmpty(phone)){
      variables.setTyped("approved", { type: "boolean", value: true});
      console.log("Form okay, approved!");
  }
  else{
      //variables.setTyped("approved", { type: "boolean", value: false});
      variables.setTyped("approved", { type: "boolean", value: true});
      variables.setTyped("message", { type: "string", value: "Post?"});
      console.log("Form not okay, not approved!!")
  }

  const typedValues = task.variables.getAllTyped();

  console.log(typedValues)

  console.log(task)

  //const variables = new Variables().setAllTyped({ foo: {value:false, type:"boolean"}});

 // await taskService.handleBpmnError(task, "BPMNError_Code", "Error message", variables);

  // Complete the task
  await taskService.complete(task, variables);
});


// susbscribe to the topic: 'set-start-date'
client.subscribe('set-start-date', async function({ task, taskService }) {
  // business logic

  const variables = new Variables();

  variables.setTyped("startdate", { type: "Date", value: new Date()});

  // Complete the task
  await taskService.complete(task, variables);
});

// susbscribe to the topic: 'set-end-date'
client.subscribe('set-end-date', async function({ task, taskService }) {
  //business logic 

  const variables = new Variables();

  variables.setTyped("enddate", { type: "Date", value: new Date()});

  // Complete the task
  await taskService.complete(task, variables);
});


// susbscribe to the topic: 'charge-card'
client.subscribe('write-to-db', async function({ task, taskService }) {
  

  var url = "mongodb://mongo:27017/";


  const name = task.variables.get('nachname');
  const vorname = task.variables.get('vorname');
  const email = task.variables.get('email');
  const phone = task.variables.get('phone');
  const startdate = task.variables.get('startdate');
  const enddate = task.variables.get('enddate');
  const tischnummer = task.variables.get('tischnummer');
  const relevantetischunummern = task.variables.get('relevantetischnummer');
  const restaurant = task.variables.get('restaurant');


  MongoClient.connect(url, function(err, db) {
    if (err) throw err;

    var dbo = db.db("gpm");
    var myobj = { 
                  name: name, 
                  vorname: vorname,
                  email: email,
                  phone: phone, 
                  restaurant: restaurant,
                  startdate: startdate, 
                  enddate: enddate, 
                  tischnummer: tischnummer,
                  rel_tische: relevantetischunummern };

    dbo.collection("corona-form-data").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });

  // Complete the task
  await taskService.complete(task);
});


// susbscribe to the topic: 'charge-card'
client.subscribe('check-db', async function({ task, taskService }) {
  

const MongoClient = require('mongodb').MongoClient;

var url = "mongodb://mongo:27017/";

async function getEntries(myobj){

        var url = "mongodb://mongo:27017/";

        const db = await MongoClient.connect(url);
        try {
            const stuff = await db.db("gpm").collection("corona-form-data").find(myobj).toArray();
        // Do something with the result of the query

            //console.log(stuff)
            return stuff;
        } finally {
            db.close();
        }
}

async function getOthersAtSameTime(startdate, enddate, tischnummern){

    var myobj = {
        "$or" : 
            [
                {
                    "enddate":{
                        '$gte': startdate,
                        '$lt': enddate
                    }, 
                },
                {
                    "startdate": {
                        '$gte': startdate,
                        '$lt': enddate 
                    }
                }
            ],
        "$and":
        [
            {"$or": tischnummern}
        ]
       
        
    };
    const db = await MongoClient.connect(url);
        try {
            const stuff = await db.db("gpm").collection("corona-form-data").find(myobj).toArray();
        // Do something with the result of the query
            console.log("test")
            return stuff
        } finally {
            db.close();
        }

}

function getRelTables(obj){
    var array = JSON.parse("[" + obj.rel_tische + "]");
    return array; 
}

function getTischnummerQueryasArray(tischnummerarray){
    query = new Array()
    for(var i=0; i<tischnummerarray.length; i++){
        var z = {
            "tischnummer":tischnummerarray[i]
        }
        query.push(z)
    }
    return query

}

const variables = new Variables();


async function main(){

    const name = task.variables.get('nachname');
    const vorname = task.variables.get('vorname');
    const email = task.variables.get('email');
    const phone = task.variables.get('phone');

    var myobj = { 
        name: name,
        vorname: vorname,
        email: email,
        phone: phone
    }

    console.log(myobj)


    var result = await getEntries(myobj)
    
    var contact_list = new Array();

    for(var i=0; i<result.length; i++){
        startdate = result[i].startdate
        enddate= result[i].enddate

        var tischnummerarr = getRelTables(result[i])
        var tabnumer = getTischnummerQueryasArray(tischnummerarr)

        var others = await getOthersAtSameTime(startdate,enddate, tabnumer); 

        contact_list.push(others)
    }

    console.log(contact_list)

    
    variables.setTyped("restaurant_contact_list", { type: "json", value: contact_list});

    //console.log(contact_list) 

      // Complete the task
    await taskService.complete(task,variables);
    
}

main()

});





