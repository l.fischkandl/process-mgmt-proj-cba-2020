const MongoClient = require('mongodb').MongoClient;

var url = "mongodb://mongo:27017/";

async function getEntries(myobj){

        var url = "mongodb://mongo:27017/";

        const db = await MongoClient.connect(url);
        try {
            const stuff = await db.db("gpm").collection("corona-form-data").find(myobj).toArray();
        // Do something with the result of the query

            //console.log(stuff)
            return stuff;
        } finally {
            db.close();
        }
}

async function getOthersAtSameTime(startdate, enddate, tischnummern){

    var myobj = {
        "$or" : 
            [
                {
                    "enddate":{
                        '$gte': startdate,
                        '$lt': enddate
                    }, 
                },
                {
                    "startdate": {
                        '$gte': startdate,
                        '$lt': enddate 
                    }
                }
            ],
        "$and":
        [
            {"$or": tischnummern}
        ]
       
        
    };
    const db = await MongoClient.connect(url);
        try {
            const stuff = await db.db("gpm").collection("corona-form-data").find(myobj).toArray();
        // Do something with the result of the query
            console.log("test")
            return stuff
        } finally {
            db.close();
        }

}

function getRelTables(obj){
    var array = JSON.parse("[" + obj.rel_tische + "]");
    return array; 
}

function getTischnummerQueryasArray(tischnummerarray){
    query = new Array()
    for(var i=0; i<tischnummerarray.length; i++){
        var z = {
            "tischnummer":tischnummerarray[i]
        }
        query.push(z)
    }
    return query

}

async function main(){

    var myobj = { 
        name: 'Sulzer',
        vorname: 'Siegfried',
        email: 'siggi.sulzer@web.de',
        phone: '02604721836',
    }


    var result = await getEntries(myobj)
    
    var contact_list = new Array();

    for(var i=0; i<result.length; i++){
        startdate = result[i].startdate
        enddate= result[i].enddate

        var tischnummerarr = getRelTables(result[i])
        var tabnumer = getTischnummerQueryasArray(tischnummerarr)

        var others = await getOthersAtSameTime(startdate,enddate, tabnumer); 

        contact_list.push(others)
    }

    console.log(contact_list) 
    
}

main()

